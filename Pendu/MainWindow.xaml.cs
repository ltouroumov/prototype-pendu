﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pendu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WordList WordList = WordList.Instance;
        private string CurrentWord;
        private List<char> FoundLetters = new List<char>();
        private List<char> FailedLetters = new List<char>();

        public MainWindow()
        {
            InitializeComponent();
            WordList.Load();
            CurrentWord = WordList.GetWord();
            Refresh();
        }

        private void getWord_Click(object sender, RoutedEventArgs e)
        {
            CurrentWord = WordList.GetWord();
            FoundLetters.Clear();
            FailedLetters.Clear();
            Refresh();
        }

        private void Refresh()
        {
            word.Content = GetMaskedWord();
            failed.Content = String.Join(" ", FailedLetters);
        }

        private string GetMaskedWord()
        {
            return String.Join(" ", CurrentWord.Select(chr => FoundLetters.Contains(chr) ? chr : '—'));
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            var chr = Convert.ToString(e.Key).First();
            if (CurrentWord.Contains(chr)) {
                FoundLetters.Add(chr);
            } else {
                FailedLetters.Add(chr);
            }

            Refresh();
        }
    }
}
