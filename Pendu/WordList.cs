﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pendu
{
    class WordList
    {
        public readonly static WordList Instance = new WordList();

        private IList<string> Words = new List<string>();

        public void Load()
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Words.txt");
            var lines = File.ReadAllLines(filePath);
            foreach (var line in lines) {
                Words.Add(line);
            }
        }

        private Random rng = new Random();
        public string GetWord()
        {
            return Words[rng.Next(0, Words.Count)].ToUpper();
        }

    }
}
